package com.ahmedmemsa.ionex;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private MoviesAdapter moviesAdapter;
    private List<Movie> movies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = findViewById(R.id.rv);
        layoutManager = new GridLayoutManager(this,2);
        moviesAdapter = new MoviesAdapter(this);
        movies = new ArrayList<>();
        moviesAdapter.setItems(movies);
        recyclerView.setAdapter(moviesAdapter);
        recyclerView.setLayoutManager(layoutManager);





        loadMovies();
    }

    private void loadMovies() {

        Ion.with(this)
                .load("https://api.themoviedb.org/3/movie/popular?api_key=8b8f975b2825d64c1d0dacd4aa432541")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        JsonArray results = result.getAsJsonArray("results");
                        Gson gson = new Gson();
                        ArrayList<Movie> list = gson.fromJson(results, new TypeToken<List<Movie>>() {
                        }.getType());
                        movies.addAll(list);
                        moviesAdapter.notifyDataSetChanged();
                    }
                });


    }


}
